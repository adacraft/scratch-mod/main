

FROM gitpod/workspace-full:latest

# install-packages is a wrapper for `apt` that helps skip a few commands in
# the docker env.
#   python2 is needed for scratch-blocks
RUN sudo install-packages \
    python2

# The last node version that is known working with adacraft is 16. Version 18
# is known to no work.
RUN bash -c 'VERSION="16.20.1" \
    && source $HOME/.nvm/nvm.sh && nvm install $VERSION \
    && nvm use $VERSION && nvm alias default $VERSION'

RUN echo "nvm use default &>/dev/null" >> ~/.bashrc.d/51-nvm-fix